import google_faces
from csvdb import truths
import unicodecsv as csv
import subprocess
import sys
import termios
import tty
from tqdm import tqdm

#START = 5000
#LIMIT = 6000
#TEST=False

#START = 6000
#LIMIT = 36000
#TEST = False

START = 100
LIMIT = 30000
TEST = True

fname = 'datasets/FEC_dataset/faceexp-comparison-data-train-public.csv'
if TEST:
    fname = 'datasets/FEC_dataset/faceexp-comparison-data-test-public.csv'

ds = google_faces.Dataset(
        fname,
        'datasets/FEC_dataset/download/',
        cache_dir='tmp/cache/FEC_dataset',
        test=TEST,
        start=START,
        limit=LIMIT)

t = truths.Truths('datasets/ground_truths.csv')
#print(t)
fname="mturk_task_from_{}_to_{}_test_{}.csv".format(START, LIMIT, TEST)

ROWS_TO_FILL=["image_url1", "image_url2", "image_url3", "image_url4", "image_url5"]

with open(fname, "wb") as w:
    writer = csv.DictWriter(w, delimiter=',', fieldnames=ROWS_TO_FILL)
    writer.writeheader()
    wr = {}
    for k, row in tqdm(ds.db.items()):
        if k in t.db:
            smile = t.db[k].get("smile", -1)
            if smile != -1:
                print("Already rated: smile is {}".format(smile))
                continue
            if t.db[k].get('invalid', ''):
                print("invalid hit")
                continue

        sourcename = row["source"].replace(
                "datasets/FEC_dataset/download",
                "https://s3-us-west-2.amazonaws.com/mturk-task-smile")
        for rt in ROWS_TO_FILL:
            if rt in wr: continue
            wr[rt] = sourcename
            break

        if len(wr) == len(ROWS_TO_FILL):
            writer.writerow(wr)
            wr = {}


#ds.download(sequential=False)#force_cut=True)

#t.merge_truths(ds.db)
print("After Update")
print(t.to_str(70))

#t.save()


