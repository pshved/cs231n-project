import numpy as np
import scipy as sp
import scipy.ndimage
import os


class Dataset(object):
    def __init__(self, image_dir, limit=-1):
        self.image_dir = image_dir
        self.limit = limit

        true_labels = [x + '.jpg' for x in "E34 E34 E35 E36 E37 E38 E39 E40".split(' ')]
        self.SMILE = 1
        self.NON_SMILE = 0

        #X = np.array([])
        #Y = np.array([])
        X = []
        Y = []

        self.truths = {}

        # Read the dataset metadata into memory:
        for fname in os.listdir(self.image_dir): 
            if fname.startswith('.') or not fname.endswith('.jpg'): continue
            full_name = self.image_dir + '/' + fname
            imr = sp.ndimage.imread(full_name)
            # Do not move the axis!
            #imr = np.moveaxis(imr, 2, 0)
            label = int(fname in true_labels) + 0

            self.truths[full_name] = {'source': full_name, 'smile': label}

            repeat = 1
            if label == self.SMILE: repeat = 5
            for _ in range(repeat):
                X += [imr]
                Y += [label]
            print("fname: %s, shape %s, value: %d, rep: %d" %(fname,
                str(X[-1].shape), Y[-1], repeat))

        self.percentage = np.count_nonzero(Y) / np.size(Y)

        # In this dataset, the height and the width of the images varies a bit
        shapes = [x.shape for x in X]
        ms = list(shapes[0])
        for s in shapes:
            for i in [0, 1, 2]:
                ms[i] = min(ms[i], s[i])

        self.normalized_shape = ms
        print(self.normalized_shape)

        nx = [x[:ms[0], :ms[1], :ms[2]] for x in X]
        nx = [x / 255.0  for x in nx]

        self.X = np.stack(nx).astype(np.float)
        self.Y = np.stack(Y)

        #idx = np.arange(self.X.shape[0])
        #np.random.shuffle(idx)
        #self.X = self.X[idx]
        #self.Y = self.Y[idx]

    def train(self, percent=1.0):
        N = self.X.shape[0]
        holdout = int((1.0 - percent) * N)
        return self.X[:-holdout], self.Y[:-holdout]

    def val(self, percent=1.0):
        N = self.X.shape[0]
        holdout = int((1.0 - percent) * N)
        return self.X[-holdout:], self.Y[-holdout:]

    def download(self):
        pass

