"""Download and interpret the Google Facial rec dataset."""
import concurrent.futures
from collections import OrderedDict
from csvdb import csvdb
import imageio
import numpy as np
import os
from os import path, makedirs
import pycurl
import stat
from tqdm import tqdm
import unicodecsv as csv
from urllib.parse import urlparse

def img_id(image):
    return image 

class Dataset(csvdb.Database):
    def __init__(self, csv_file, final_dir, cache_dir='', limit=-1, start=-1, test=False):
        super().__init__("/nonexistant file", hint="Google Faces")
        self.csv_file = csv_file
        self.cache_dir = cache_dir
        self.final_dir = final_dir
        self.limit = limit
        self.start = start
        self.is_test_dataset = 1 if test else 0

        # Read the dataset metadata into memory:
        records_read = 0

        def make_record(url, x1, x2, y1, y2):
            o = urlparse(url)
            # since o.path starts with '/', doing path.join the whole way would obliterate the first two paths.
            src = path.join(self.final_dir, o.netloc) + o.path
            return {
                'source': src,
                'ox1': float(x1),
                'oy1': float(y1),
                'ox2': float(x2),
                'oy2': float(y2),
                'url': url,
                'test': self.is_test_dataset,
                # -1 means "unknown"
                'smile': -1,
            }

        with open(self.csv_file, 'rb') as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                records_read += 1
                if self.start >= 0 and records_read < self.start: continue

                # make a DB row.
                self.merge_record(make_record(*row[0:5]))
                self.merge_record(make_record(*row[5:10]))
                self.merge_record(make_record(*row[10:15]))
                        
                if self.limit >= 0 and records_read >= self.limit: break

        print("Total number of records: {}", len(self.db))


    def save(self):
        raise Exception("save() not supported on Google Faces dataset.")

    def download(self, cache_dir=None, force_dl=False, force_cut=False, debug=False, sequential=False):
        if not cache_dir: cache_dir = self.cache_dir
        if cache_dir and self.final_dir.endswith('/') and not cache_dir.endswith('/'):
            cache_dir += '/'

        def exists_and_nonempty(fname):
            try:
                sr = os.stat(fname)
                return sr.st_size > 0 and stat.S_ISREG(sr.st_mode)
            except IOError:
                return False

        def do_download(url, target):
            """Actually downloads source URL to the target file."""
            makedirs(path.dirname(target), exist_ok=True)
            with open(target, 'wb') as f:
                c = pycurl.Curl()
                c.setopt(c.URL, url)
                c.setopt(c.WRITEDATA, f)
                #c.setopt(c.HTTPHEADER, ["Cookie: %s" % COOKIE])
                c.setopt(c.FOLLOWLOCATION, True)
                c.perform()
                c.close()

        def do_crop(source, target, record):
            """Actually downloads source URL to the target file."""
            makedirs(path.dirname(target), exist_ok=True)
            im = imageio.imread(source)
            #import ipdb; ipdb.set_trace()
            if len(im.shape) == 2:
                im = np.tile(np.expand_dims(im, axis=2), 3)
                record['grayscale'] = 1
            elif len(im.shape) == 3:
                record['grayscale'] = 0
            else:
                raise Exception("Can't parse image at {}".format(source))
            Y, X, _ = im.shape
            x1 = int(X * record['ox1'])
            x2 = int(X * record['ox2'])
            y1 = int(Y * record['oy1'])
            y2 = int(Y * record['oy2'])
            x1, x2 = min(x1, x2), max(x1, x2)
            y1, y2 = min(y1, y2), max(y1, y2)
            cropped = im[y1:y2, x1:x2, :]
            if debug: print("Cropping {} --> {} from {}".format(im.shape, cropped.shape, source))
            imageio.imwrite(target, cropped)

            return record


        def do_all(record):
            dl_target = record['source'].replace(self.final_dir, cache_dir)
            target = record['source']

            # Download, if needed
            if not exists_and_nonempty(dl_target) or force_dl:
                if not cache_dir:
                    raise Exception('Cache dir required to download files.')
                url = record['url']
                if debug: print("Downloading {} --> {}".format(url, dl_target))
                try:
                    do_download(url, dl_target)
                except (pycurl.error, IsADirectoryError, FileExistsError) as ve:
                    record['invalid'] = 1
                    record['invalid_reason'] = str(ve)
                    if debug: print("Marking {} as invalid during download: {}!".format(dl_target, ve))
                    return

            # Crop if needed
            if not exists_and_nonempty(target) or force_cut:
                # This will tag the image as "grayscale"
                try:
                    do_crop(dl_target, target, record)
                except (ValueError, IOError) as ve:
                    record['invalid'] = 1
                    record['invalid_reason'] = str(ve)
                    if debug: print("Marking {} as invalid: {}!".format(target, ve))

        if sequential:
            for record in tqdm(self.db.values()):
                do_all(record)
            return

        # Concurrent
        with concurrent.futures.ThreadPoolExecutor(max_workers=10) as e:
            results = list(tqdm(e.map(do_all, self.db.values()), total=len(self.db)))

