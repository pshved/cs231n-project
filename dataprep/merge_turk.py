import google_faces
from csvdb import truths, csvdb
import subprocess
import sys
import termios
import tty
from tqdm import tqdm
import unicodecsv as csv

START = 5000
LIMIT = 6000

#fname='datasets/turk/Batch_3655966_batch_results.csv'
#fname='datasets/turk/Batch_3655959_batch_results.csv'
fname='datasets/turk/Batch_3655873_batch_results.csv'

results = {}

FIELD_MAPS = {
        'Input.image_url': ["Answer.category","Answer.category.label"],
        'Input.image_url1': ["Answer.category","Answer.category.label"],
        'Input.image_url2': ["Answer.category2","Answer.category2.label"],
        'Input.image_url3': ["Answer.category3","Answer.category3.label"],
        'Input.image_url4': ["Answer.category4","Answer.category4.label"],
        'Input.image_url5': ["Answer.category5","Answer.category5.label"]}

def maybe_merge(row, urlf, af, result):
    if urlf not in row: return False
    if af not in row: return False
    smilevalue = int(result.get("smile", "-1"))
    if smilevalue != -1: return True

    sourcename = row[urlf].replace(
            "https://s3-us-west-2.amazonaws.com/mturk-task-smile",
            "datasets/FEC_dataset/download")
    result.update({'source': sourcename})

    a = row[af]

    if a == "Face obstructed":
        smilevalue = -2
    elif a == "Not smiling / Unsure":
        smilevalue = 0
    elif a == "Smiling":
        smilevalue = 1
    else:
        return False
    result['smile'] = smilevalue
    return True

    # That's ok, just keep going.


with open(fname, 'rb') as fh:
    reader = csv.DictReader(fh, delimiter=',')
    #import ipdb; ipdb.set_trace()
    for row in reader:
        result = {}
        for url_field, answer_fields in FIELD_MAPS.items():
            for answer_field in answer_fields:
                done = maybe_merge(row, url_field, answer_field, result)
                if done:
                    results[result['source']] = result
                    result = {}

t = truths.Truths('datasets/ground_truths.csv')
t.merge(results)

#import ipdb; ipdb.set_trace()

print(t)


#ds.download(sequential=False)#force_cut=True)

#t.merge_truths(ds.db)
print("After Update")
print(t.to_str(70))

t.save()


