#!/usr/bin/python
"""Curler downloads pages from the web based on a file."""

import pycurl
import sys
import time
import random

COOKIE_HEADER = "cookie.txt"

def read_cookie():
    with open(COOKIE_HEADER, "r") as f:
        return f.readline().rstrip()
    return ''

#COOKIE = read_cookie()

inp = sys.argv[1]
outdir = sys.argv[2]

if inp == '':
    raise "No input to crawl!"

if outdir == '':
    raise "Nowhere to save!"

def build_list(inp, outdir):
    l = []
    with open(inp, "r") as f:
        for line in f:
            line = line.rstrip()
            if not line: continue
            fn, url = line.split(" ", 2)  
            l += [("%s/%s" % (outdir, fn), url)]
    return l

def curl_url(url, fname):
    with open(fname, 'wb') as f:
        c = pycurl.Curl()
        c.setopt(c.URL, url)
        c.setopt(c.WRITEDATA, f)
        c.setopt(c.HTTPHEADER, ["Cookie: %s" % COOKIE])
        c.perform()
        c.close()

l = build_list(inp, outdir)
for fn, url in l:
    print "curl %s > %s" % (url, fn)
    time.sleep(random.uniform(1.5, 3.5))
    curl_url(url, fn)


