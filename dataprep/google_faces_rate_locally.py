import google_faces
from csvdb import truths
import subprocess
import sys
import termios
import tty
from tqdm import tqdm

ds = google_faces.Dataset(
        'datasets/FEC_dataset/faceexp-comparison-data-train-public.csv',
        'datasets/FEC_dataset/download/',
        cache_dir='tmp/cache/FEC_dataset',
        limit=220)

t = truths.Truths('datasets/ground_truths.csv')
print(t)

def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(fd)
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch

def rate(row):
    quit = False
    if row.get("smile", -1) != -1:
        print("existing smile: {}".format(row.get("smile", -1)))
        return row, False
    if row.get("invalid", ""):
        print("Rating invalid row {} as -2".format(row['source']))
        row["smile"] = -2
        return row, False
    print("rating {}".format(row['source']))
    subprocess.check_call(["ristretto", row["source"]])
    valid_inp = False
    while not valid_inp:
        print("rate image {} (N=not smiling, J=smiling, K=unknown, Z=quit)".format(row['source']))
        c = getch()
        print("Got: {}".format(c))
        valid_inp = True
        if c == "n":
            row["smile"] = 0
        elif c == "j":
            row["smile"] = 1
        elif c == "k":
            row["smile"] = -2
        elif c == "z":
            quit = True
        else:
            valid_inp = False

    return row, quit

#import ipdb; ipdb.set_trace()
for k, row in tqdm(ds.db.items()):
    if k in t.db:
        # Update the existing row.
        row, quit = rate(t.db[k])
    else:
        # Update the new row.
        row, quit = rate(row)
    if quit:
        break
    t.merge_record(row)


#ds.download(sequential=False)#force_cut=True)

#t.merge_truths(ds.db)
print("After Update")
print(t.to_str(70))

t.save()


