import google_faces
from csvdb import truths

# ds = google_faces.Dataset(
#         'datasets/FEC_dataset/faceexp-comparison-data-train-public.csv',
#         'datasets/FEC_dataset/download/',
#         cache_dir='tmp/cache/FEC_dataset',
#         limit=300000)

ds = google_faces.Dataset(
        'datasets/FEC_dataset/faceexp-comparison-data-test-public.csv',
        'datasets/FEC_dataset/download/',
        cache_dir='tmp/cache/FEC_dataset',
        test=True,
        limit=30000)

ds.download(sequential=False)#force_cut=True)

t = truths.Truths('datasets/ground_truths.csv')
print(t)
t.merge_truths(ds.db)
print("After Update")
print(t.to_str(10))

t.save()


