import imageio
from PIL import Image
import numpy as np
import pickle

im = imageio.imread("testdata/me_smile.jpg")
pil_im = Image.fromarray(im, mode='RGB')
pil_res = pil_im.resize((300, 224))

img = np.array(pil_res)

with open("testdata/me_smile.pickle", "wb") as w:
    pickle.dump(img, w)

