# Prepare input camera image for inference using raspberry pi.
import numpy as np
import os
from os.path import realpath, normpath, dirname
from PIL import Image

from smilebot import imageops

class Provider(object):
    def __init__(self, use_vj, classifier_base=None, classifier_path='data/haarcascade_frontalface_default.xml', vj_scale=1.2, debug=False):
        self.use_vj = use_vj
        self.vj_scale = vj_scale
        self.debug = debug
        if self.use_vj:
            import cv2 as cv
            model_dir = classifier_base
            if model_dir is None:
                model_dir = normpath(dirname(cv.__file__))
            model_fname = os.path.join(model_dir, classifier_path)
            print("Loading VJ model from {}...".format(model_fname))
            self.face_cascade = cv.CascadeClassifier(model_fname)


    def for_pi(self, img, norm_shape, debuginfo):
        if self.use_vj:
            return self.viola_jones_first(img, norm_shape, debuginfo)
        else:
            return self.regular(img, norm_shape, debuginfo)

    def regular(self, img, norm_shape, debuginfo, force_noresize=True):
        print("Rescaling...")
        img = imageops.rescale_to_crop(img, norm_shape, force_noresize=force_noresize)
        print(img.shape)
        print(img.dtype)
        print("Inferring...")

        fname = 'processed_{}'.format(debuginfo)
        if self.debug:
            save_img = Image.fromarray(img.astype(np.uint8))
            save_img.save(fname)
            print("saved to {}".format(fname))

        img = img / 255.0

        return img

    def viola_jones_first(self, img, norm_shape, debuginfo):
        print("Img size: {} {} ".format(*img.shape))
        import cv2 as cv
        # Detect face.  If a face was detected, crop to the face.  Otherwise, return the original image.
        grayscale_image = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        detected_faces = self.face_cascade.detectMultiScale(grayscale_image)

        if not len(detected_faces):
            return self.regular(img, norm_shape, debuginfo)

        for (c,r,w,h) in detected_faces:
            print("Detected: {} {} , {} {}".format(c,r,c+w,r+h))

            # Let's zoom out of the face a bit.  We don't want to accidentally crop it out.
            mwh = max(w,h)  # Resize to fit.
            mwh *= self.vj_scale      # Add 10% from the sides.
            c -= (mwh-w) / 2
            w += (mwh-w) / 2
            r -= (mwh-h) / 2
            h += (mwh-h) / 2

            c = int(max(0, c))
            r = int(max(0, r))
            w = int(min(img.shape[1], w))
            h = int(min(img.shape[0], h))

            print("After resize: {} {} , {} {}".format(c,r,c+w,r+h))

            # These are transposed...
            img = img[r:r+h, c:c+w, :]
            print("Face size: {} {} ".format(*img.shape))

            # The face is usually smaller; we have to resize
            return self.regular(img, norm_shape, debuginfo, force_noresize=False)
