import RPi.GPIO as GPIO
import time
import picamera
import picamera.array

from rpi import base
from smilebot import imageops
from io import BytesIO
from PIL import Image

DEFAULT_PINS = {
        'take': 18,
        'yes': 22,
        'no': 23,
}

KEEP_SMILE_SECONDS = 3
KEEP_CAPTURES = 20

class Buttons(base.ButtonsBase):
    def __init__(self, pins):
        super().__init__()
        self.pins = pins
        self.test = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pins['take'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.pins['yes'], GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.pins['no'], GPIO.OUT, initial=GPIO.LOW)

    def yes(self):
        def do_yes():
            GPIO.output(self.pins['no'], GPIO.LOW)
            GPIO.output(self.pins['yes'], GPIO.HIGH)
            time.sleep(KEEP_SMILE_SECONDS)
            GPIO.output(self.pins['yes'], GPIO.LOW)
            print("Yes!")
        return do_yes

    def no(self):
        def do_no():
            GPIO.output(self.pins['yes'], GPIO.LOW)
            GPIO.output(self.pins['no'], GPIO.HIGH)
            time.sleep(KEEP_SMILE_SECONDS)
            GPIO.output(self.pins['no'], GPIO.LOW)
            print("No!")
        return do_no

    def err(self):
        def do_err(err):
            print("Err!")
        return do_err

    def Run(self):
        print("Starting wait-and-infer loop...")
        while True:
            input_state = GPIO.input(self.pins['take'])
            if self.test:
                time.sleep(3)
                input_state = False
            if not input_state:
                print("Button pressed!")
                self.on_press()


class Camera(object):
    def __init__(self, debug=False):
        self.debug = debug
        self.capture_idx = 0
        self.cam = picamera.PiCamera()
        # TODO: reduce this as much as you can!
        #self.cam.resolution = (320, 240)
        #self.cam.resolution = (320, 224)
        #self.cam.resolution = (1280, 720)
        self.cam.resolution = (640, 480)

    def capture_and(self, cont):
        # Continuation
        def ret():
            img, debuginfo = self.capture()
            return cont(img, debuginfo)
        return ret

    def capture(self):
        # Invoke that command-line utility to capture the image.
        with picamera.array.PiRGBArray(self.cam) as output:
            self.cam.capture(output, 'rgb')
            print('Captured image {}'.format(output.array.shape))
            fname = 'capture{}.jpg'.format(self.capture_idx)
            self.capture_idx += 1
            if self.capture_idx > KEEP_CAPTURES: self.capture_idx = 0
            if self.debug:
                save_img = Image.fromarray(output.array)
                save_img.save(fname)
            return output.array, fname


    def badcapture():
        stream = BytesIO()
        # TODO:  change format to bytes
        self.cam.capture(stream, format='jpeg')
        stream.seek(0)
        pil_img = Image.open(stream)
        return [1,2,3]


def do_nothing():
    pass

def do_nothing_arg(ignore_argument):
    pass

class FakeInference(object):
    def __init__(self, modelfile, threshold=0.5, on_true=do_nothing, on_false=do_nothing, on_error=do_nothing_arg):
        self.fns = {}
        self.on_true = on_true
        self.on_false = on_false
        self.on_error = on_error

        self.threshold = threshold

    def on_start(self):
        # Start he model inference
        def do_start(img):
            try:
                # Preprocessing
                result = self.infer(img)

                if result:
                    #self.fns['on_true']()
                    self.on_true()
                else:
                    self.on_false()
            except Exception as e:
                self.on_error(e)
                if self.test:
                    raise(e)

        return do_start

    def infer(self, img):
        print("Rescaling...")
        norm_shape = (224, 224)
        img = imageops.rescale_to_crop(img, norm_shape, force_noresize=True)
        print(img.shape)
        print(img.dtype)

        img = img / 255.0
        print(img[0][0])

        print("No model; returning true")
        return True


