"""Base classes for Smile Bot"""
from abc import ABC, abstractmethod


def _do_sleep_a_bit():
    time.sleep(0.44)


class ButtonsBase(ABC):
    def __init__(self):
        super().__init__()
        self.on_press = _do_sleep_a_bit

    def yes(self):
        def do_yes():
            print("Yes!")
        return do_yes

    def no(self):
        def do_no():
            print("No!")
        return do_no

    def err(self):
        def do_err(err):
            print("Err!")
        return do_err

    @abstractmethod
    def Run(self):
        pass

