"""Bindings for running the smile bot inference in non-prod environment."""
import os
import imageio


from rpi import base

# This class must follow the interface
class EvaluateFolder(base.ButtonsBase):
    def __init__(self, folder, fake_camera):
        self.folder = folder
        self.fake_camera = fake_camera

    def err(self):
        def do_err(err):
            raise err
        return do_err

    def Run(self):
        print("Running fake eval in {}".format(self.folder))
        for dp, dn, files in os.walk(self.folder):
            for f in sorted(files):
                fn = os.path.join(dp, f)
                print("walking {}".format(f))
                self.fake_camera.set_next_image(fn)
                self.on_press()

class FakeCamera(object):
    def __init__(self):
        self.next_image = None
        self.next_debuginfo = None

    def set_next_image(self, file_name):
        self.next_image = imageio.imread(file_name)
        self.next_debuginfo = file_name

    def capture_and(self, on_start):
        def ret():
            return on_start(self.next_image, self.next_debuginfo)
        return ret


