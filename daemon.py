import argparse

import tflib_lite
from rpi import devel, prepare

parser = argparse.ArgumentParser(description='Run the Smiling Robot on device or in test mode')
parser.add_argument('--dry_run', action='store_true', help='evaluate the smile bot on the images in the captures/ folder')
parser.add_argument('--use_vj_detector', action='store_true', help='use Viola-Jones face detector to prepare')
parser.add_argument('--debug', action='store_true', help='save the captures to disk')

args = parser.parse_args()

# Do not leave the border around the image as the training data was passed through vj.
provider = prepare.Provider(args.use_vj_detector, debug=args.debug, vj_scale=1.0, classifier_base='vj/')

if args.dry_run:
    camera = devel.FakeCamera()
    buttons = devel.EvaluateFolder('captures/', camera)
else:
    from rpi import pilib
    pin_config = pilib.DEFAULT_PINS
    buttons = pilib.Buttons(pin_config)
    camera = pilib.Camera(debug=args.debug)

#inference = tflib.Inference('', on_true=buttons.yes(), on_false=buttons.no(), on_error=buttons.err())
#inference = pilib.FakeInference('', on_true=buttons.yes(), on_false=buttons.no(), on_error=buttons.err())
default_kwargs = {
        'feature_provider': provider,
        'on_true': buttons.yes(),
        'on_false': buttons.no(),
        'on_error': buttons.err(),
}
inference = tflib_lite.Inference('lite_models/one_out_of_8_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-182130.savedmodel', threshold=0.4, **default_kwargs)
#inference = tflib_lite.Inference('lite_models/one_out_of_4_layers-sm-2-20190602-170215.savedmodel', threshold=0.45, **default_kwargs)
#inference = tflib_lite.Inference('lite_models/one_out_of_3_layers-20190601-160320.savedmodel', threshold=0.45, **default_kwargs)
#inference = tflib.Inference()

buttons.on_press = camera.capture_and(inference.on_start())

#buttons.test = True
buttons.Run()

