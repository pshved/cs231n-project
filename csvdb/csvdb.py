"""Base class for a CSV-based database."""
import collections
import unicodecsv as csv

NO_DATA = '<no entries>'
SOURCE_ITEM = 'source'

MAXPRINT = 10

class Database(object):
    def __init__(self, csv_filename, limit=-1, hint='CSVdb', source_label=SOURCE_ITEM):
        """All entries in csv_filename should be int, except for the "source"."""
        self.filename = csv_filename
        self.limit = limit
        self.hint=hint
        self.SOURCE_ITEM=source_label

        self.db = collections.OrderedDict()
        try:
            def convert_types(row):
                try:
                    row['smile'] = int(row.get('smile', -1))
                except ValueError:
                    row['smile'] = -1
                return row
            records_read = 0
            with open(self.filename, 'rb') as fh:
                reader = csv.DictReader(fh, delimiter=',')
                for row in reader:
                    self.db[row[self.SOURCE_ITEM]] = convert_types(row)
                    records_read += 1
                    if self.limit >= 0 and records_read >= self.limit: break
        except FileNotFoundError:
            pass


    def __str__(self):
        return self.to_str(MAXPRINT)

    def to_str(self, maxprint):
        to_read = maxprint
        left = max(0, len(self.db) - to_read)
        r = self.hint
        for k, v in self.db.items():
            if to_read <= 0: break
            to_read -= 1
            r += '\nFile {} --> {}'.format(k, {i:v[i] for i in v if i != self.SOURCE_ITEM})
        if left > 0:
            r += '\n... and {} more'.format(left)

        if not self.db: r += '\n' + NO_DATA

        return r


    def merge(self, truths, allowed_keys=None):
        for k, v in truths.items():
            self.merge_one(k, v, allowed_keys)

    def merge_record(self, row, allowed_keys=None):
        return self.merge_one(row['source'], row, allowed_keys)

    def merge_one(self, k, v, allowed_keys=None):
        src = k
        if src not in self.db: self.db[src] = {}
        if allowed_keys is not None:
            for kk, vv in v.items():
                if kk not in allowed_keys: continue
                self.db[src][kk] = vv
        else:
            self.db[src].update(v)
        if k not in self.db[src]:
            self.db[src][self.SOURCE_ITEM] = k

    def save(self, to_file=''):
        if not to_file: to_file = self.filename
        flds = {self.SOURCE_ITEM: True}
        for _, v in self.db.items():
            for k in v:
                flds[k] = True
        with open(to_file, 'wb') as fh:
            writer = csv.DictWriter(fh, delimiter=',', fieldnames=flds.keys())
            writer.writeheader()
            for _, v in self.db.items():
                writer.writerow(v)

