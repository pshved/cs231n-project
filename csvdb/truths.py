"""A class that represents Ground Truths."""
from collections import OrderedDict
import os
import unicodecsv as csv
from csvdb import csvdb

class Truths(csvdb.Database):
    def __init__(self, csv_filename):
        super(Truths, self).__init__(csv_filename, hint='Truths')

    def merge_truths(self, truths):
        return self.merge(truths)

    def filtered_db(self, lkv=lambda k, v: True):
        """Return db where k and v match lkv(k, v)."""
        return OrderedDict([(k, v) for k, v in self.db.items() if lkv(k, v)])

