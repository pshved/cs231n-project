import numpy as np
import pickle

import tflib_lite

def load_file():
    import imageio
    from PIL import Image
    im = imageio.imread("testdata/me_smile.jpg")
    #im = imageio.imread("testdata/me_frown.jpg")
    #im = imageio.imread("testdata/cut_smile.jpg")
    pil_im = Image.fromarray(im, mode='RGB')
    pil_res = pil_im.resize((300, 224))

    img = np.array(pil_res)
    return img

def load_pickle():
    with open("testdata/me_smile.pickle", "rb") as r:
        return pickle.load(r)

#img = load_file()
img = load_pickle()

#inf = tflib.Inference('models/one_out_of_8_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-182130.savedmodel', threshold=0.4)
inf = tflib_lite.Inference('lite_models/one_out_of_8_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-182130.savedmodel', threshold=0.4)
#inf = tflib.Inference('models/experiment-20190601-133359.savedmodel', threshold=0.4)
#inf = tflib.Inference('models/one_out_of_3_layers-20190601-160320.savedmodel', threshold=0.4)
r = inf.infer(img)
print("Inference result: {}".format(r))
