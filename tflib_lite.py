import tensorflow.keras.backend as K
import tensorflow as tf
import numpy as np

from rpi import prepare

# Load the needed model from the modelfile and prepare it for inference.


def do_nothing():
    pass

def do_nothing_arg(ignored):
    pass

class Inference(object):
    def __init__(self, modelfile, feature_provider=None, threshold=0.5, on_true=do_nothing, on_false=do_nothing, on_error=do_nothing_arg):
        self.fns = {}
        self.on_true = on_true
        self.on_false = on_false
        self.on_error = on_error

        self.provider = feature_provider

        self.threshold = threshold

        self.interpreter = tf.lite.Interpreter(model_path=modelfile)
        self.interpreter.allocate_tensors()


    def on_start(self):
        # Start he model inference
        def do_start(img, debuginfo):
            try:
                # Preprocessing
                result = self.infer(img, debuginfo)

                if result:
                    #self.fns['on_true']()
                    self.on_true()
                else:
                    self.on_false()
            except Exception as e:
                self.on_error(e)

        return do_start

    def infer(self, img, debug):
        norm_shape = (224, 224)
        img = self.provider.for_pi(img, norm_shape, debug)
        print(img[0][0])

        img = np.expand_dims(img, axis=0)

        #img = (img * 255).astype(np.uint8)
        img = img.astype(np.float32)
        inp_d = self.interpreter.get_input_details()
        out_d = self.interpreter.get_output_details()
        self.interpreter.set_tensor(inp_d[0]['index'], img)
        self.interpreter.invoke()
        px = self.interpreter.get_tensor(out_d[0]['index'])

        class_scores = px[0]

        print("Got result: {} for image {}".format(class_scores, debug))

        return class_scores[1] >= self.threshold


