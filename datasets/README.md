`ground_truths.csv` contains ~7000 images from the Google Facial Emotion Comparison dataset labeled with "smiling / not smiling" data.

* 1 means "smiling"
* 0 means "not smiling"
* -1 means "not yet labeled"
* -2 means "invalid / unknown / photo doesn't exist"
