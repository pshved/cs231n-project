# This particular file isn't really used; instead, use tflib_lite.py
import tensorflow.keras.backend as K
import tensorflow as tf
import numpy as np

from smilebot import imageops

# Load the needed model from the modelfile and prepare it for inference.


def do_nothing():
    pass

class Inference(object):
    def __init__(self, modelfile, threshold=0.5, on_true=do_nothing, on_false=do_nothing, on_error=do_nothing):
        self.fns = {}
        self.on_true = on_true
        self.on_false = on_false
        self.on_error = on_error

        self.threshold = threshold

        self.model = None
        if modelfile:
            print("Initializing tf session...")
            self.sess = K.get_session()
            init = tf.global_variables_initializer()

            self.sess.run(init)

            print("Loading model from {}...".format(modelfile))
            self.model = tf.keras.models.load_model(modelfile, compile=True)
            self.model._make_predict_function()
        print("Model loaded.")

    def on_start(self):
        # Start he model inference
        def do_start(img):
            try:
                # Preprocessing
                result = self.infer(img)

                if result:
                    #self.fns['on_true']()
                    self.on_true()
                else:
                    self.on_false()
            except:
                self.on_error()

        return do_start

    def infer(self, img):
        print("Rescaling...")
        norm_shape = (224, 224)
        img = imageops.rescale_to_crop(img, norm_shape, force_noresize=True)
        print(img.shape)
        print(img.dtype)

        img = img / 255.0
        print(img[0][0])

        if not self.model:
            print("No model; returning true")
            return True

        print("Inferring...")

        img = np.expand_dims(img, axis=0)
        ti = tf.convert_to_tensor(img, dtype=tf.float32)
        # For some reason, the trained model comes in form of a numpy array--well, convert it back.
        res = tf.convert_to_tensor(self.model.predict(ti, steps=1), dtype=tf.float32)

        sess = tf.Session()
        px = sess.run(res)

        class_scores = px[0]

        print("Got result: {}".format(class_scores))

        return class_scores[1] >= self.threshold


