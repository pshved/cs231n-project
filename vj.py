import cv2 as cv

# Read image from your local file system
original_image = cv.imread('captures/capture0.jpg')

# Convert color image to grayscale for Viola-Jones
grayscale_image = cv.cvtColor(original_image, cv.COLOR_BGR2GRAY)

face_cascade = cv.CascadeClassifier('/home/cs231n/raspberry_pi_env/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')
detected_faces = face_cascade.detectMultiScale(grayscale_image)

for (c,r,w,h) in detected_faces:
    print("Detected: {} {} , {} {}".format(c,r,c+w,r+h))

