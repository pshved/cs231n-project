# As usual, a bit of setup
from tqdm import tqdm
import datetime
import numpy as np
import math
import matplotlib.pyplot as plt
import tensorflow as tf
import math
import pickle
import timeit
import os

from tensorboard import summary as summary_lib

from smilebot import augmentations, features, metrics, focal_loss, alexnet_like, profile
from csvdb import truths

#from tensorflow.python import debug as tf_debug
#import tensorflow.keras.backend as K
#sess = K.get_session()
#sess = tf_debug.LocalCLIDebugWrapperSession(sess)
#K.set_session(sess)

np.random.seed(410)

plt.rcParams['figure.figsize'] = (10.0, 8.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

# for auto-reloading external modules
# see http://stackoverflow.com/questions/1907993/autoreload-of-modules-in-ipython

def rel_error(x, y):
  """ returns relative error """
  return np.max(np.abs(x - y) / (np.maximum(1e-8, np.abs(x) + np.abs(y))))


profiles = profile.ProfileStore('results/profiles.csv')
pr_curves = profile.PRCurve('results/curves.csv')
t = truths.Truths('datasets/ground_truths.csv')
a = augmentations.Augmentations('datasets/augmentations.csv', t, equalize_method='crop', choice="weak")

def feature_filter(source, row):
    #if '/jacfee/' not in row['source']: return False;
    return row['smile'] >= 0 and not row.get('invalid', False)
p = features.Provider(t, a, norm_shape=(224, 224), truths_filter=features.and_not_test(feature_filter))
p_test = features.Provider(t, a, norm_shape=(224, 224), truths_filter=features.and_test(feature_filter))

from PIL import Image

def imshow_no_ax(img, normalize=True):
    """ Tiny helper to show images as uint8 and remove axis labels """
    if normalize:
        img_max, img_min = np.max(img), np.min(img)
        img = 255.0 * (img - img_min) / (img_max - img_min)
    plt.imshow(img.astype('uint8'))
    plt.gca().axis('off')

#imshow_no_ax(ds.X[11], normalize=True)
#print(ds.Y[11])
logs_base_dir = "logs"
models_base_dir = "models"
vals_base_dir = "valsets"

dirs = {
        'logs_base': logs_base_dir,
        'models_base': models_base_dir,
        'vals_base': vals_base_dir,
}

_, _, X_test, y_test, test_provenance = p_test.train_test(train_percent=0, num_augmentations=0, shuffle=False,
        howmany=1000)

fraclayer = 6
stridemul = 2
fracfc = 2
drop_rate = 0.5
train_kwargs = {
        'silence_tb': False,
        #'load_from': 'patient_models/experiment-20190530-214947.savedmodel',
        #'load_from': 'patient_models/experiment-20190601-112756.savedmodel',

        # Best baseline model
        #'load_from': 'models/experiment-20190601-133359.savedmodel',

        # Best overall model
        #'load_from': 'models/one_out_of_3_layers-20190601-160320.savedmodel',

        #'load_from': 'models/one_out_of_4_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-175725.savedmodel',

        #'load_from': 'models/one_out_of_1_layers-nobn1-20190602-142703.savedmodel',

        # Most embedded model
        #'load_from': 'models/one_out_of_6_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-183014.savedmodel',


        # this one is (25%)
        #'load_from': 'models/one_out_of_4_layers-sm-2-20190602-170215.savedmodel',
        # actually well-performing model (20%)
        #'load_from': 'models/one_out_of_3_layers-sm-220190602-162257.savedmodel',

        # Smallest model
        'load_from': 'models/one_out_of_8_layers-sm-2-ffc-2d0.5-xl:3c4c-20190602-182130.savedmodel',

        'eval_only': True,
        #'eval_pr_curve': True,
        #'threshold': 0.01,
        #'export_only': True,

        'learning_rate': 5e-2,
        #'learning_rate': 1e-2,
        'loss': 'cxe',

        #'custom_model_init_fn': lambda: alexnet_like.Model(X_test.shape, name='experiment-', dirs=dirs),
        #'custom_model_init_fn': lambda: alexnet_like.Model(X_test.shape, name='experiment-', dirs=dirs),

        # No Schedule:
        #'learning_rate': 2e-2,
        #'schedule': False,
        'num_epochs': 20,
}
X_train, y_train, X_val, y_val, provenance = None, None, None, None, None
if not train_kwargs.get('eval_only', False):
    X_train, y_train, X_val, y_val, provenance = p.train_test(train_percent=0.8,
            num_augmentations=5, shuffle=False,
            equalize_test=(not (train_kwargs.get('loss', 'cxe') == 'focal')),
            shift=0, 
            #howmany=10)
            #howmany=500)
            howmany=5000)
else:
    # Jus load something...
    X_train, y_train, X_val, y_val, provenance = p.train_test(train_percent=0.8,
            num_augmentations=5, shuffle=False,
            howmany=10)

flat_pixels = X_val.reshape(-1, 3)
avgs = np.average(flat_pixels, axis=0)
std = np.std(flat_pixels, axis=0)

print("Average pixel value: " + str(avgs))
print("Std pixel dev: " + str(std))

print("Preparation done, let's train!")
print("Random choice accuracy for validation is {}".format(np.average(y_val)))

#X_train, y_train = ds.train(percent=0.7)
#X_val, y_val = ds.val(percent=0.7)
device = '/device:GPU:0'   # Change this to a CPU/GPU as you wish!
#device = '/cpu:0'        # Change this to a CPU/GPU as you wish!
print(X_train.shape)

try:
    gpus = tf.config.experimental.list_physical_devices('GPU')
    # Restrict TensorFlow to only allocate 9GB of memory on the first GPU
    tf.config.experimental.set_virtual_device_configuration(
	gpus[0],
	[tf.config.experimental.VirtualDeviceConfiguration(memory_limit=9 * 1024)])
except:
    pass

#logs_base_dir = "./logs_patient"
#models_base_dir = "./patient_models"
#vals_base_dir = "./patient_valsets"

class CustomConvNet(tf.keras.Model):
    def __init__(self, name='', fraction=1, stridemul=1):
        super(CustomConvNet, self).__init__()

        num_classes = 2
        initializer = tf.initializers.VarianceScaling(scale=2.0)
        reg_rate = 1e-3
        layers = [
            tf.keras.layers.Conv2D(
                input_shape=X_train.shape[1:],
                #filters=int(max(32, 96 / fraclayer)),
                filters=int(96 / fraclayer),
                strides=(4 * stridemul, 4 * stridemul),
                kernel_size=11,
                padding="valid",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(
                filters=int(256 / fraction),
                strides=(1,1),
                kernel_size=5,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            tf.keras.layers.BatchNormalization(),
#            tf.keras.layers.Conv2D(
#                filters=int(384 / fraction),
#                kernel_size=3,
#                padding="same",
#                activation='relu',
#                kernel_initializer=initializer,
#                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
#            tf.keras.layers.Conv2D(
#                filters=int(384 / fraction),
#                kernel_size=3,
#                padding="same",
#                activation='relu',
#                kernel_initializer=initializer,
#                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.Conv2D(
                filters=int(256 / fraction),
                kernel_size=3,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dropout(rate=drop_rate),
            tf.keras.layers.Dense(int(1024 / fracfc), activation='softmax',
                                  kernel_initializer=initializer,
                                  kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            #tf.keras.layers.GlobalAveragePooling2D(),
            tf.keras.layers.Dense(num_classes, activation='softmax',
                                  kernel_initializer=initializer,
                                  kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate))
        ]
        self.model = tf.keras.Sequential(layers)
                                        
        self.savename = name + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.logdir = os.path.join(logs_base_dir, self.savename)
        self.modelfile = os.path.join(models_base_dir, self.savename + '.savedmodel')
        self.valfile = os.path.join(vals_base_dir, self.savename + '.pickle')
        print(self.logdir)
        self.tensorboard_callback = tf.keras.callbacks.TensorBoard(
            self.logdir, histogram_freq=1)
        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                            END OF YOUR CODE                              #
        ############################################################################

    def call(self, input_tensor, training=False):
        ############################################################################
        # TODO: Construct a model that performs well on CIFAR-10                   #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        #import ipdb; ipdb.set_trace()
        #print(input_tensor)
        vs = self.model(input_tensor)#, trainig=training)
        return vs

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                            END OF YOUR CODE                              #
        ############################################################################

        return x

print_every = 700

def c_model_init_fn():
    return CustomConvNet("one_out_of_{}_layers-sm-{}-ffc-{}d{}-xl:3c4c-".format(fraclayer,stridemul, fracfc, drop_rate), fraclayer, stridemul)

def c_optimizer_init_fn():
    learning_rate = 1e-3
    return tf.keras.optimizers.Adam(learning_rate) 

#train_part34(model_init_fn, optimizer_init_fn, gnum_epochs=num_epochs, is_training=True,
#            callback=lambda m: callback(m))

global_model = 2


def train_seq(model_init_fn, optimizer_init_fn, num_epochs=1, is_training=False, callback=None, **kwargs):
    do_train = not kwargs.get('eval_only', False)
    silence_tb = kwargs.get('silence_tb', False) or not do_train
    # This function keeps the learning rate at 0.001 for the first ten epochs
    # and decreases it exponentially after that.
    def scheduler(base_lr, decay_after, total_epochs):
        def sch(epoch):
            if epoch < decay_after:
                return base_lr
            else:
                return base_lr * math.exp(0.1 * (decay_after - epoch))
        def cosine(epoch):
            if epoch < decay_after:
                lr = float(epoch + 1 ) / (decay_after + 1) * base_lr
            else:
                lr = 0.5 * base_lr * (1 + math.cos(math.pi * epoch / total_epochs))
            print("Using learning rate {}".format(lr))
            return lr
        return cosine

    lr = kwargs.get('learning_rate', 5e-3)
    num_epochs = kwargs.get('num_epochs', num_epochs)
    do_schedule = kwargs.get('schedule', True)
    #lr = 1e-3

    loss_fn = tf.losses.SparseCategoricalCrossentropy()
    lf_name = kwargs.get('loss', 'cxe')
    if lf_name == 'focal':
        print("Using focal loss.")
        #loss_fn = focal_loss.categorical_focal_loss()
        #loss_fn = focal_loss.binary_focal_loss(gamma=0)
        loss_fn = focal_loss.categorical_focal_loss(gamma=2)

    if 'custom_model_init_fn' in kwargs:
        print("Using custom model init")
        model_init_fn = kwargs['custom_model_init_fn']

    g = tf.Graph()
    eval_g = g  # I couldn't run the eval on a different graph sadly

    global global_model
    def do(use_test_set, do_train, load_from, kwargs):
        global global_model
        val_results = []
        val_names = []

        modelfile = load_from
        threshold = kwargs.get('threshold', 0.5)

        with tf.device(device):
            model = model_init_fn()

            X, y, prov = X_val, y_val, provenance
            if use_test_set:
                X, y, prov = X_test, y_test, test_provenance

            total_positives = tf.cast(tf.math.count_nonzero(y_test), tf.float32)
            total_negatives = tf.cast(tf.math.count_nonzero(1 - y_test), tf.float32)

            # Metrics
            mcs = [tf.keras.metrics.SparseCategoricalAccuracy(),
                    metrics.PrecisionForClass(klazz=1, thresholds=threshold),
                    metrics.RecallForClass(klazz=1, thresholds=threshold),
                    metrics.AUCForClass(klazz=1)]
            if kwargs.get('eval_pr_curve', False):
                mcs += [
                    metrics.FalsePositivesForClass(total=total_negatives, klazz=1, thresholds=threshold),
                    metrics.TruePositivesForClass(total=total_positives, klazz=1, thresholds=threshold)]

            val_names = [m.name for m in mcs]
            val_names = ['loss'] + val_names

            model.compile(
                          #optimizer=tf.keras.optimizers.Adam(2e-2),
                          optimizer=tf.keras.optimizers.SGD(learning_rate=lr, nesterov=True, momentum=0.80),
                          #loss='sparse_categorical_crossentropy',
                          #loss=tf.losses.SparseCategoricalCrossentropy(),#from_logits=True),
                          #loss=focal_loss.single_categorical_focal_loss(),
                          loss=loss_fn,
                          metrics=mcs)


            if load_from:
                model.model = tf.keras.models.load_model(load_from)
                model.modelfile = load_from
                model.valfile = load_from.replace("models", "valsets", 1).replace("savedmodel", "pickle")
                print("Loaded model from {}".format(load_from))

            global_model = model
            if kwargs.get('export_only', False):
                return

            print("Shapes: {} and {}".format(X_val.shape, y_val.shape))

            callbacks = []
            if not silence_tb:
                callbacks.append(model.tensorboard_callback)
            if do_schedule:
                callbacks.append(tf.keras.callbacks.LearningRateScheduler(scheduler(lr, 10, num_epochs)))

            eval_num = 0
            if not use_test_set:
                if do_train:
                    model.fit(
                            X_train.astype(np.float32),
                            y_train,
                            batch_size=64, epochs=num_epochs,
                            validation_data=(X_val.astype(np.float32), y_val),
                            callbacks=callbacks)
                    model.model.save(model.modelfile, include_optimizer=True)
                    print("Saved model to {}".format(model.modelfile))
                    modelfile = model.modelfile
            print("Exporting model")

            val_results = model.evaluate(X.astype(np.float32), y)
            eval_num = y.shape[0]
            print_val(-1, 4, model, tf.losses.SparseCategoricalCrossentropy(), None, None, debug=True, provenance=prov[1], save=model.valfile, X_val=X, y_val=y)


        #eval_profiles = not kwargs.get('eval_pr_curve')
        eval_profiles = False
        eval_curves = kwargs.get('eval_pr_curve')

        if (not do_train) and eval_profiles:
            profiles.for_model(model.savename, model.modelfile)

            results = dict(zip(val_names, val_results))

            if not use_test_set:
                profiles.measure(eval_g, 'val', eval_num, results)
                #profiles.measure(g, 'train', X_train.shape[0])
            else:
                profiles.measure(eval_g, 'test', eval_num, results)

            profiles.save()

        # Evaluate a point on a p/r curve
        if (not do_train) and eval_curves:
            pr_curves.for_model(model.savename, model.modelfile)

            results = dict(zip(val_names, val_results))

            if not use_test_set:
                pr_curves.measure(eval_g, 'val', eval_num, threshold, results)
            else:
                pr_curves.measure(eval_g, 'test', eval_num, threshold, results)

            profiles.save()

        return modelfile

    load_from = kwargs.get('load_from', '')
    if do_train:
        load_from = do(False, True, load_from, kwargs)

    # Evaluate.
    if kwargs.get('eval_pr_curve', False):
        # Evaluating pr curve.
        num_thresholds = kwargs.get('eval_pr_curve_thresholds', 11)
        ts = [x * 1.0 / (num_thresholds - 1) for x in range(num_thresholds)]
        # Log-discretize border thresholds
        tx = 1.0/(num_thresholds - 1)
        for i in range(5):
            tx /= 2
            ts.append(tx)
            ts.append(1-tx)

        ts = sorted(ts)
        for t in ts:
            copy_kwargs = dict(kwargs)
            copy_kwargs['threshold'] = t
            do(not do_train, False, load_from, copy_kwargs)
        pr_curves.save()
    else:
        # Evaluating normally, auc and ops
        #with g.as_default():
            do(not do_train, False, load_from, kwargs)



hidden_size, num_classes = 30, 10
learning_rate = 1e-2

def model_init_fn():
    return TwoLayerFC(hidden_size, num_classes)

def optimizer_init_fn():
    #return tf.keras.optimizers.SGD(learning_rate=learning_rate)
    return tf.keras.optimizers.Adam(learning_rate=learning_rate)

def print_val(t, epoch, model, loss_fn, train_loss, train_accuracy, debug=False, provenance=[], save='', loss_fns=[], X_val=X_val, y_val=y_val):
    prec = tf.metrics.Precision()
    recall = tf.metrics.Recall()
    auc = tf.metrics.AUC()

    val_acc = np.average(y_val)
    if train_loss is None:
        train_loss = tf.metrics.SparseCategoricalCrossentropy()
    if train_accuracy is None:
        train_accuracy = tf.metrics.SparseCategoricalAccuracy()


    val_loss = tf.metrics.SparseCategoricalCrossentropy()
    val_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='val_accuracy')

    save_things = {
            'val_examples': [],
    }

    for test_x, test_y in [(X_val, y_val)]:
        # During validation at end of epoch, training set to False
        prediction = tf.cast(model(test_x.astype(np.float32), training=False), dtype=tf.float32)
        t_loss = loss_fn(test_y, prediction)

        if debug:
            template = 'Val Answers: {}\t\tExpected acc: {}'#\n Val Predictions: {}'
            acc = 1.0 - float(np.count_nonzero(test_y)) / np.size(test_y)
            print (template.format(test_y, acc))#, prediction))

        for i in range(len(test_y)):
            y = test_y[i]
            p = prediction[i, :]
            yp = int(prediction[i, 1] > 0.5)
            prov = None
            if provenance: prov = provenance[i]
            ok = y == yp
            save_things['val_examples'].append({
                'i': i,
                'y': y,
                'yp': yp,
                'pred': p,
                'source': prov,
                'ok': ok,
            })
            okstr = " OK " if ok else "FAIL"
            if debug:
                print ('Example {}, {}: y={}, y\'={}, pred={}, key={}'.format(i, okstr, y, yp, p, prov))#, prediction))

        val_loss.update_state(test_y, prediction)
        val_accuracy.update_state(test_y, prediction)

        #import ipdb; ipdb.set_trace()
        prec.update_state(test_y, prediction[:, 1] > 0.5)
        recall.update_state(test_y, prediction[:, 1] > 0.5)
        auc.update_state(test_y, prediction[:, 1])

    template = 'Iteration {}, Epoch {}, Loss: {}, Accuracy: {}, Val Loss: {}, Val Accuracy: {} (compare with {}), prec = {}, recall = {}, auc = {}'
    print (template.format(t, epoch+1,
                         train_loss.result(),
                         train_accuracy.result()*100,
                         val_loss.result(),
                         val_accuracy.result()*100, val_acc,
                         prec.result(), recall.result(), auc.result()))
    val_loss.reset_states()
    val_accuracy.reset_states()

    if save:
        pickle.dump(save_things, open(save, "wb"))

def train_part34(model_init_fn, optimizer_init_fn, num_epochs=1,
        is_training=False, callback=None, print_every=700, debug=False,
        batch_size=16, debug_val=False, val_acc=-1.0):
    """
    Simple training loop for use with models defined using tf.keras. It trains
    a model for one epoch on the CIFAR-10 training set and periodically checks
    accuracy on the CIFAR-10 validation set.

    Inputs:
    - model_init_fn: A function that takes no parameters; when called it
      constructs the model we want to train: model = model_init_fn()
    - optimizer_init_fn: A function which takes no parameters; when called it
      constructs the Optimizer object we will use to optimize the model:
      optimizer = optimizer_init_fn()
    - num_epochs: The number of epochs to train for

    Returns: Nothing, but prints progress during trainingn
    """    
    with tf.device(device):

        # Compute the loss like we did in Part II
        loss_fn = tf.keras.losses.SparseCategoricalCrossentropy()

        model = model_init_fn()
        optimizer = optimizer_init_fn()

        # Metrics
        prec = tf.metrics.Precision()
        recall = tf.metrics.Recall()
        auc = tf.metrics.AUC()

        train_loss = tf.metrics.SparseCategoricalCrossentropy()
        train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

        val_loss = tf.metrics.SparseCategoricalCrossentropy()
        val_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='val_accuracy')

        xep = np.array_split(X_train, batch_size)
        yep = np.array_split(y_train, batch_size)
        t = 0


        print_val(t, -1, model, loss_fn, train_loss, train_accuracy, debug=debug or debug_val)

        for epoch in tqdm(range(num_epochs)):

            # Reset the metrics - https://www.tensorflow.org/alpha/guide/migration_guide#new-style_metrics
            train_loss.reset_states()
            train_accuracy.reset_states()

            idx = np.random.choice(np.arange(X_train.shape[0]), batch_size)
            x_np = X_train[idx]
            y_np = y_train[idx]

            with tf.GradientTape() as tape:

                # Use the model function to build the forward pass.
                scores = tf.cast(model(x_np, training=is_training),
                        dtype=tf.float32)
                loss = loss_fn(y_np, scores)
                if debug:
                    template = 'Test Answers: {}\n Test Predictions: {}'
                    print (template.format(y_np, scores))

                gradients = tape.gradient(loss, model.trainable_variables)
                optimizer.apply_gradients(zip(gradients, model.trainable_variables))

                # Update the metrics
                train_loss.update_state(y_np, scores)
                train_accuracy.update_state(y_np, scores)

                if callback:
                    callback(model)

                if t % print_every == 0:
                    print_val(t, epoch, model, loss_fn, train_loss, train_accuracy, debug=debug or debug_val)

                t += 1


class TwoLayerFC(tf.keras.Model):
    def __init__(self, hidden_size, num_classes):
        super(TwoLayerFC, self).__init__()        
        raise Exception("Don't do this!")
        initializer = tf.initializers.VarianceScaling(scale=2.0)
        self.fc1 = tf.keras.layers.Dense(hidden_size, activation='relu',
                                   kernel_initializer=initializer)
        reg_rate = 1e-2
        self.conv1 = tf.keras.layers.Conv2D(
                filters=16,
                strides=(8, 8),
                kernel_size=32,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate))
        self.conv2 = tf.keras.layers.Conv2D(
                filters=10,
                strides=(1, 1),
                kernel_size=8,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate))
        self.drop = tf.keras.layers.Dropout(rate=0.6)
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.conv3 = tf.keras.layers.Conv2D(
                filters=10,
                strides=(1, 1),
                kernel_size=8,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate))
        self.mp = tf.keras.layers.MaxPool2D()
        self.fc2 = tf.keras.layers.Dense(num_classes, activation='softmax',
                                   kernel_initializer=initializer)
        self.flatten = tf.keras.layers.Flatten()
    
    def call(self, x, training=False):
        #import ipdb; ipdb.set_trace()
        #x = self.flatten(x)
        x = tf.cast(x, tf.float16)
        x = self.conv1(x)
        x = self.drop(x, training=training)
        x = self.conv2(x)
        #x = self.bn1(x)
        #x = self.conv3(x)
        #x = self.mp(x)
        x = self.flatten(x)
        x = self.fc2(x)
        return x

hidden_size, num_classes = 4000, 2
learning_rate = 1e-2

def model_init_fn():
    return TwoLayerFC(hidden_size, num_classes)

def optimizer_init_fn():
    learning_rate = 1e-4
    return tf.keras.optimizers.Adam(learning_rate=learning_rate)

print (np.count_nonzero(y_val) / np.size(y_val))
#train_part34(model_init_fn, optimizer_init_fn, num_epochs=1000, print_every=50, debug_val=True, debug=False, val_acc = 0.8, batch_size=128)

print(len(p.truths.db))
train_seq(c_model_init_fn, c_optimizer_init_fn, **train_kwargs)

#%tensorboard --logdir {logs_base_dir}
#%tensorboard --logdir {model.logdir}


