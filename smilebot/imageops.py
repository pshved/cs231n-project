import numpy as np
from PIL import Image

def rescale_to_crop(img, target_shape, force_noresize=False):
    Y, X, c = img.shape
    tY, tX = target_shape

    sY = float(tY) / Y
    sX = float(tX) / X

    s = max(sX, sY)

    # In case scaling produced a slightly smaller box, ensure it's at least the desired size.
    actual_tY = max(tY, int(Y * s))
    actual_tX = max(tX, int(X * s))

    resized = img
    # Only resize when necessary
    if not force_noresize and (tX != X or tY != Y):
        orig = Image.fromarray(img.astype(np.uint8))
        # NB: axes are swapped here!
        resized = orig.resize((actual_tX, actual_tY))

    # Now, center-crop
    t = (-tY + actual_tY) / 2
    l = (-tX + actual_tX) / 2
    final = None
    if isinstance(resized, np.ndarray):
        cropped = resized[int(t):int(t+tY), int(l):int(l+tX), :]
        final = np.asarray(cropped, dtype=np.float)
    else:
        # It's an Image object
        # NB: axes are swapped here!
        cropped = resized.crop((l, t, l+tX, t+tY))
        final = np.asarray(cropped, dtype=np.float)
    assert final.shape[0] == tY
    assert final.shape[1] == tX
    return final

