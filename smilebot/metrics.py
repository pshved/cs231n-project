import tensorflow as tf

class PrecisionForClass(tf.keras.metrics.Precision):

    def __init__(self, name='precision_for_class', klazz=0, **kwargs):
        super().__init__(name=name, **kwargs)
        self.klazz=klazz

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred[:, self.klazz:self.klazz+1]
        super(PrecisionForClass, self).update_state(y_true, y_pred)


class RecallForClass(tf.keras.metrics.Recall):

    def __init__(self, name='recall_for_class', klazz=0, **kwargs):
        super().__init__(name=name, **kwargs)
        self.klazz=klazz

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred[:, self.klazz:self.klazz+1]
        super(RecallForClass, self).update_state(y_true, y_pred)

class AUCForClass(tf.keras.metrics.AUC):

    def __init__(self, name='auc_for_class', klazz=0, **kwargs):
        super().__init__(name=name, **kwargs)
        self.klazz=klazz

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred[:, self.klazz:self.klazz+1]
        super(AUCForClass, self).update_state(y_true, y_pred)

class FalsePositivesForClass(tf.keras.metrics.FalsePositives):

    def __init__(self, total=1, name='false_pos_for_class', klazz=0, **kwargs):
        super().__init__(name=name, **kwargs)
        self.klazz=klazz
        self.total = total

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred[:, self.klazz:self.klazz+1]
        super(FalsePositivesForClass, self).update_state(y_true, y_pred, sample_weight=1.0/self.total)

class TruePositivesForClass(tf.keras.metrics.TruePositives):

    def __init__(self,total=1,  name='true_pos_for_class', klazz=0, **kwargs):
        super().__init__(name=name, **kwargs)
        self.klazz=klazz
        self.total = total

    def update_state(self, y_true, y_pred, sample_weight=None):
        y_pred = y_pred[:, self.klazz:self.klazz+1]
        super(TruePositivesForClass, self).update_state(y_true, y_pred, sample_weight=1.0/self.total)
