import augmentations
import features
from csvdb import truths
from dataprep import jacfee_dataset
import numpy as np

t = truths.Truths('datasets/ground_truths.csv')
print(t)

#t.merge_truths({'item1': {'source': 'item1', 'smile': 1, 'unrateable': False}})
# t.merge_truths({'item2': {'source': 'item2', 'smile': 1,}})

ds = jacfee_dataset.Dataset('datasets/jacfee')
t.merge_truths(ds.truths)
print("After Update")
print(t)

t.save()

a = augmentations.Augmentations('datasets/augmentations.csv', t)
print(a)

np.random.seed(410)

p = features.Provider(t, a, norm_shape=(597, 859))

print(p.shuffled_keys)

X_tr, y_tr, X_t, y_t = p.train_test()

print(X_tr.shape)
print(y_tr.shape)
print(X_t.shape)
print(y_t.shape)
