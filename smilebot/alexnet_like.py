import datetime
import os
import tensorflow as tf

class Model(tf.keras.Model):
    def __init__(self, shape, name='', dirs={}):
        # Shape is required for save/restore model to work.
        super(Model, self).__init__()

        logs_base_dir = dirs['logs_base']
        models_base_dir = dirs['models_base']
        vals_base_dir = dirs['vals_base']

        num_classes = 2
        initializer = tf.initializers.VarianceScaling(scale=2.0)
        reg_rate = 1e-3
        layers = [
            tf.keras.layers.Conv2D(
                input_shape=shape[1:],
                filters=96,
                strides=(4, 4),
                kernel_size=11,
                padding="valid",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(
                filters=256,
                strides=(1,1),
                kernel_size=5,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            #tf.keras.layers.Dropout(rate=0.5),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(
                filters=384,
                kernel_size=3,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.Conv2D(
                filters=384,
                kernel_size=3,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.Conv2D(
                filters=256,
                kernel_size=3,
                padding="same",
                activation='relu',
                kernel_initializer=initializer,
                kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            tf.keras.layers.MaxPool2D(pool_size=3, strides=(2,2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(1024, activation='softmax',
                                  kernel_initializer=initializer,
                                  kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate)),
            #tf.keras.layers.GlobalAveragePooling2D(),
            tf.keras.layers.Dense(num_classes, activation='softmax',
                                  kernel_initializer=initializer,
                                  kernel_regularizer=tf.keras.regularizers.l2(l=reg_rate))
        ]
        self.model = tf.keras.Sequential(layers)
                                        
        self.savename = name + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.logdir = os.path.join(logs_base_dir, self.savename)
        self.modelfile = os.path.join(models_base_dir, self.savename + '.savedmodel')
        self.valfile = os.path.join(vals_base_dir, self.savename + '.pickle')
        print(self.logdir)
        self.tensorboard_callback = tf.keras.callbacks.TensorBoard(
            self.logdir, histogram_freq=1)
        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                            END OF YOUR CODE                              #
        ############################################################################

    def call(self, input_tensor, training=False):
        ############################################################################
        # TODO: Construct a model that performs well on CIFAR-10                   #
        ############################################################################
        # *****START OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****

        #import ipdb; ipdb.set_trace()
        #print(input_tensor)
        vs = self.model(input_tensor)#, trainig=training)
        return vs

        # *****END OF YOUR CODE (DO NOT DELETE/MODIFY THIS LINE)*****
        ############################################################################
        #                            END OF YOUR CODE                              #
        ############################################################################

        return x

