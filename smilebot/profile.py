import os
from csvdb import csvdb
import tensorflow as tf
from google.protobuf import text_format

class ProfileStore(csvdb.Database):
    def __init__(self, csv_filename, dirname="profile"):
        super().__init__(csv_filename)
        self.dirname = dirname

    def for_model(self, savename, modelfile):
        self.savename = savename
        self.modelfile = modelfile

    def make_key(self, g, which_set, eval_num, extras={}):
        if not self.modelfile:
            raise Exception("Call for_model() before calling measure()")

        return "{}-{}-{}".format(self.modelfile.replace('/', '-'), which_set, eval_num)

    def measure(self, g, which_set, eval_num, extras={}):
        key = self.make_key(g, which_set, eval_num)

        # Collect the profile from the graph
        opts_dict = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()
        fname = os.path.join("profile", key + ".summary.txt")
        proto_fname = os.path.join("profile", key + ".textproto")
        opts = (tf.compat.v1.profiler.ProfileOptionBuilder(opts_dict)
            .with_file_output(fname)
            .build())

        #import ipdb; ipdb.set_trace()
        #opts['select'].append('bytes')
        #opts['select'].append('micros')
        prof = tf.compat.v1.profiler.Profiler(g)
        gnp = prof.profile_operations(opts)
        flops = gnp.total_float_ops
        print("Total float ops: {} ".format(flops))
        with open(proto_fname, "w") as f:
            f.write(text_format.MessageToString(gnp))

        result = {
                'model': self.modelfile,
                'eval_count': eval_num,
                'flops': flops,
                'file': fname,
                'proto_file': proto_fname,
                'set': which_set,
                'num_parameters':  gnp.total_parameters,
                #'peak_bytes': gnp.total_peak_bytes,
                #'res_bytes': gnp.total_residual_bytes,
                #'micros': gnp.total_exec_micros,
        }
        result.update(extras)

        self.merge_one(key, result)

class PRCurve(ProfileStore):
    def __init__(self, csv_filename, dirname="profile"):
        super().__init__(csv_filename)

    def measure(self, g, which_set, eval_num, threshold, extras={}):
        key = self.make_key(g, which_set, eval_num) + '-{}'.format(threshold)

        result = {
                'model': self.modelfile,
                'set': which_set,
                'threshold': threshold,
        }
        result.update(extras)

        self.merge_one(key, result)
