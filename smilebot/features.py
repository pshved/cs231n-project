"""Provides of the features."""
import numpy as np
import scipy as sp   # For imread.
from tqdm import tqdm

def and_not_test(fn):
    def ant(source, row):
        return not row.get('test', False) and fn(source,row)
    return ant

def and_test(fn):
    def ant(source, row):
        return row.get('test', False) and fn(source,row)
    return ant

class Provider(object):
    def __init__(self, truths, augmentations, truths_filter=lambda k,v: True, normalize_class='smile', norm_shape=None):
        self.truths = truths
        self.truths_filter = truths_filter
        self._cached_db = None
        self.aug = augmentations
        self.normalize_class = normalize_class
        self.norm_shape = norm_shape

        # This will fill up shuffled_keys
        self.shuffled_keys = self.make_shuffled_keys(self.db())
        self.keys = self.make_shuffled_keys(self.db(), shuffle=False)
        #self.normalize_and_shuffle()

    def db(self):
        if self._cached_db: return self._cached_db
        self._cached_db = self.truths.filtered_db(self.truths_filter)
        return self._cached_db

    def make_shuffled_keys(self, db, shuffle=True):
        keys = np.array(list(db.keys()))
        if shuffle:
            np.random.shuffle(keys)
        return keys

    def get_shuffled_keys(self, shuffle=True):
        if shuffle:
            return self.shuffled_keys
        else:
            return self.keys

    def normalize_and_shuffle(self, keys, shuffle=True):
        # Let's compute how many items for every class
        trues = np.array([], dtype=np.str)
        falses = np.array([], dtype=np.str)

        for k in keys:
            row = self.db()[k]
            if row[self.normalize_class]:
                trues = np.append(trues, k)
            else:
                falses = np.append(falses, k)

        if len(trues) == 0: return []

        # For each fals, there'll be this many trues
        true_coeff = float(len(falses)) / len(trues)
        assert true_coeff >= 1.0, "Sorry, there must be more false than true samples... found {} vs {}".format(len(trues), len(falses))

        # Shuffle here
        #np.random.shuffle(trues)
        #np.random.shuffle(falses)

        X = []

        i = -1
        need_falses = 0.0
        got_falses = 0.0
        for fk in trues:
            # Sample trues with fraction
            need_falses += true_coeff
            while got_falses < need_falses - 1.01:
                i += 1
                got_falses += 1.0
                X.append(falses[i])
                X.append(fk)

        # Shuffle again
        if shuffle:
            np.random.shuffle(X)

        return X

    def select_load_and_return(self, fri, ti, equalize=False, shuffle=True):
        ks = self.get_shuffled_keys(shuffle)[fri:ti]
        print("Need to load {} images (unbalanced)...".format(len(ks)))
        return self.load_and_return(ks, equalize=equalize, shuffle=shuffle)

    def load_and_return(self, ks, equalize=False, shuffle=True):
        if equalize:
            ks = self.normalize_and_shuffle(ks, shuffle=shuffle)
        imgs = []
        features = []
        provs = []
        print("Loading {} images (balanced)...".format(len(ks)))
        for k in tqdm(ks):
            img = self.aug.normalize_image(sp.ndimage.imread(k), self.norm_shape)
            imgs.append(img)
            provs.append(k)
            features.append(self.db()[k])
        if imgs:
            return np.stack(imgs), features, provs
        else:
            return np.array([], dtype=np.uint8), [], []


    def train_test(self, feature='smile', howmany=-1, shift=0, train_percent=1.0, num_augmentations=0, shuffle=True, equalize_test=True):
        """Args:
            
            percent: what fraction to put into the training set.
        """
        if howmany < 0: howmany = len(self.db())
        print("Getting {} features from {}".format(howmany, shift))
        holdout = int((1.0 - train_percent) * howmany)
        train_fri = shift
        train_ti = shift + howmany - holdout
        test_fri = train_ti
        test_ti = shift + howmany

        print("Will create {} train and {} test images before augmentations".format(train_ti - train_fri, test_ti - test_fri))

        # Compile arrays.
        X_tr, y_tr, prov_X_tr = self.select_load_and_return(train_fri, train_ti, equalize=equalize_test,
                shuffle=shuffle)
        y_tr = np.array([f[feature] for f in y_tr])
        X_test, y_test, prov_X_test = self.select_load_and_return(test_fri, test_ti, equalize=False,
                shuffle=shuffle)
        y_test = np.array([f[feature] for f in y_test])

        # Augment test data
        if num_augmentations > 0:
            print("Augmenting each image {} times...".format(num_augmentations))
            N = X_tr.shape[0]
            aug_X = np.repeat(X_tr, num_augmentations , axis=0)
            aug_y = np.repeat(y_tr, num_augmentations , axis=0)
            prov_X_tr = np.repeat(prov_X_tr, num_augmentations , axis=0)

            #import ipdb; ipdb.set_trace()
            self.aug.perform(aug_X[N:])

            print("Shuffling...")
            X_tr = aug_X
            y_tr = aug_y
            #X_tr = np.concatenate((X_tr, aug_X), axis=0)
            #y_tr = np.concatenate((y_tr, aug_y), axis=0)
            # Shuffle
            if shuffle:
                pmo = np.random.permutation(len(y_tr))
                X_tr = X_tr[pmo]
                prov_X_tr = X_tr[pmo]
                y_tr = y_tr[pmo]
                print("Shuffle done.")
            print("Shuffle not requested.")

        # Accept "holdout"
        return X_tr, y_tr, X_test, y_test, (prov_X_tr, prov_X_test)

    def from_keys(self, keys, equalize=False, num_augmentations=0):
        """Provide the features based on a set of keys."""
        return self.load_and_return(keys, equalize=equalize)


