"""A class that generates, stores, and manages augmentations."""
import os
from PIL import Image
import unicodecsv as csv

import numpy as np
from csvdb import csvdb
from smilebot import imageops

#from tqdm import tqdm_notebook as tqdm
from tqdm import tqdm

from albumentations import (
    HorizontalFlip, IAAPerspective, ShiftScaleRotate, CLAHE, RandomRotate90,
    Transpose, ShiftScaleRotate, Blur, OpticalDistortion, GridDistortion, HueSaturationValue,
    IAAAdditiveGaussianNoise, GaussNoise, MotionBlur, MedianBlur, IAAPiecewiseAffine,
    IAASharpen, IAAEmboss, RandomBrightnessContrast, Flip, OneOf, Compose,
    RGBShift
)


NO_DATA = '<no entries>'
SOURCE_ITEM = 'source'

class Augmentations(csvdb.Database):
    def __init__(self, csv_filename, truths, automerge=True, equalize_method='none', choice="strong"):
        """
        Params:
            - equalize_method: `none`, or `crop`
        """
        super(Augmentations, self).__init__(csv_filename, hint='Augmentations')
        self.truths = truths
        self.equalize_method = equalize_method
        self.choice = choice

        self.AUGMENTATIONS = []

        if automerge:
            self.merge(self.truths.db, allowed_keys=self.AUGMENTATIONS)

    def perform(self, X):
        def strong_aug(p=0.5):
            return Compose([
                #RandomRotate90(),
                #Flip(),
                #Transpose(),
                OneOf([
                    IAAAdditiveGaussianNoise(),
                    GaussNoise(),
                ], p=0.2),
                MotionBlur(blur_limit=40, p=0.99),
                OneOf([
                    MotionBlur(p=0.2),
                    MedianBlur(blur_limit=3, p=0.1),
                    Blur(blur_limit=3, p=0.1),
                ], p=0.2),
                ShiftScaleRotate(shift_limit=0.625, scale_limit=0.25, rotate_limit=35, p=0.2),
                OneOf([
                    OpticalDistortion(p=0.3),
                    GridDistortion(p=0.1),
                    IAAPiecewiseAffine(p=0.3),
                ], p=0.2),
                OneOf([
                    CLAHE(clip_limit=2),
                    IAASharpen(),
                    IAAEmboss(),
                    RandomBrightnessContrast(),
                ], p=0.3),
                HueSaturationValue(p=0.3),
            ], p=p)

        def weak_aug(p=0.5):
            return Compose([
                #RandomRotate90(),
                #Flip(),
                #Transpose(),
                OneOf([
                    IAAAdditiveGaussianNoise(),
                    GaussNoise(),
                ], p=0.2),
                MotionBlur(blur_limit=5, p=0.85),
                MotionBlur(blur_limit=10, p=0.3),
                MotionBlur(blur_limit=40, p=0.1),
                OneOf([
                    MotionBlur(p=0.2),
                    MedianBlur(blur_limit=3, p=0.1),
                    Blur(blur_limit=3, p=0.1),
                ], p=0.2),
                ShiftScaleRotate(shift_limit=0.225, scale_limit=0.25, rotate_limit=25, p=0.2),
                OneOf([
                    OpticalDistortion(p=0.3),
                    GridDistortion(p=0.1),
                    IAAPiecewiseAffine(p=0.3),
                ], p=0.2),
                OneOf([
                    CLAHE(clip_limit=2),
                    IAASharpen(),
                    IAAEmboss(),
                    RandomBrightnessContrast(),
                ], p=0.3),
                RandomBrightnessContrast(p=0.5),
                HueSaturationValue(p=0.3),
            ], p=p)


        for i in tqdm(range(X.shape[0])):
            augmentation = strong_aug
            if self.choice == "weak":
                augmentation = weak_aug
            xi = self.denormalize_image(X[i])
            xj = augmentation(p=0.95)(image=xi)["image"]
            X[i, :] = self.normalize_image(xj, shape=False)
        return X

    def denormalize_image(self, x):
        nx = x * 255.0
        nx = nx.astype(np.uint8)
        return nx


    def normalize_image(self, x, norm_shape=None, shape=True):
        nx = x.astype(np.float)
        if shape:
            if self.equalize_method == 'crop':
                # nx = nx[:norm_shape[0], :norm_shape[1], :]
                nx = imageops.rescale_to_crop(nx, norm_shape)
            elif self.equalize_method == 'none':
                nx = nx[:norm_shape[0], :norm_shape[1], :]
            else:
                raise ValueError('Bad equalize_method: {}'.format(self.equalize_method))
        # Remove 4th channel if exists
        nx = nx / 255.0
        if nx.shape[2] == 4:
            # Multiply by alpha and be done with it.
            nx *= np.expand_dims(nx[:, :, 3], axis=2)
            nx = nx[:, :, :3]

        return nx







