import numpy as np

# These are actual means and std from our dataset. 
SQUEEZENET_MEAN = np.array([0.61076548, 0.55462388, 0.51144325], dtype=np.float32)
SQUEEZENET_STD = np.array([0.27786946, 0.24724257, 0.24213955], dtype=np.float32)

def preprocess_image(img, div=False):
    """Preprocess an image for squeezenet.
    
    Subtracts the pixel mean and divides by the standard deviation.
    """
    mpl = 1.0
    if div: mpl /= 255.0
    return (img.astype(np.float32) * mpl - SQUEEZENET_MEAN) / SQUEEZENET_STD

def deprocess_image(img, rescale=False, div=False):
    """Undo preprocessing on an image and convert back to uint8."""
    mpl = 1.0
    if div: mpl *= 255.0
    img = (img * SQUEEZENET_STD + SQUEEZENET_MEAN)
    if rescale:
        vmin, vmax = img.min(), img.max()
        img = (img - vmin) / (vmax - vmin)
    clipped = np.clip(mpl * img, 0.0, 1.0*mpl)
    if div:
        return clipped.astype(np.uint8)
    return clipped
